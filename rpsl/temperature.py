#!/usr/bin/env python3
# -*- coding: utf-8 -*-

"""
Read temperature on a RaspberryPi with a DS18B20.

On Raspbian, the 1-wire interface have to be enabled to use this module.
(use raspi-config command to enable the 1-wire interface)

This module is based on
https://github.com/simonmonk/raspberrypi_cookbook_ed2/blob/master/temp_DS18B20.py

See also
--------

"The Raspberry Pi Cookbook" by Simon Monk Ed. Oreilly recipe 12.9 (1st edition)
"""

import datetime
import glob    # Unix style pathname pattern expansion
import os
import time

DEFAULT_LOG_FILE_PATH = "~/temperature.csv"
SLEEP_TIME = 60      # seconds
DATA_BUFFER_SIZE = 60


def _get_raw_data():
    '''Get raw data

    /sys/bus/w1/devices/28*/w1_slave
    '''

    base_dir = '/sys/bus/w1/devices/'

    if not os.path.exists(base_dir):
        err_msg = base_dir + " not found. Be sure the 1-wire interface " \
                  "is enabled (use raspi-config for Raspbian users " \
                  "otherwise use 'modprobe w1-gpio' and 'modprobe w1-therm')"
        raise Exception(err_msg)

    device_folder = glob.glob(base_dir + '28*')[0]
    device_file = device_folder + '/w1_slave'
    # print(device_file)

    with open(device_file, 'r') as fd:
        lines = fd.readlines()

    return lines

 
def get_temperature_degree_celsius():
    '''Get formatted data'''

    lines = _get_raw_data()

    while lines[0].strip()[-3:] != 'YES':
        time.sleep(0.2)
        lines = _get_raw_data()

    equals_pos = lines[1].find('t=')

    if equals_pos != -1:
        temp_string = lines[1][equals_pos+2:]
        temp_c = float(temp_string) / 1000.0

        return temp_c


def get_temperature_degree_fahrenheit():
    '''Get formatted data'''

    temp_c = get_temperature_degree_celsius()
    temp_f = temp_c * 9.0 / 5.0 + 32.0

    return temp_f

        
def log_temperature(data_path=None):
    '''Log data'''

    if data_path is not None:
        data_path = os.path.expanduser(data_path)  # to handle "~/..." paths
        data_path = os.path.abspath(data_path)     # to handle relative paths
        data_buffer = []

    while True:
        dt = datetime.datetime.now().isoformat()
        temperature_c = get_temperature_degree_celsius()

        if data_path is not None:
            data_buffer.append("{},{}".format(dt, temperature_c))

            if len(data_buffer) > DATA_BUFFER_SIZE:
                with open(data_path, "a") as fd:
                    for line in data_buffer:
                        print(line, file=fd)
                data_buffer = []

        print("{} {}°C".format(dt, temperature_c))
        time.sleep(SLEEP_TIME)


if __name__ == '__main__':
    log_temperature(data_path=DEFAULT_LOG_FILE_PATH)
