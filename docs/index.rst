===========================
Raspberry Pi Sensor Library
===========================

A Raspberry Pi Sensor Library

Note:

    This project is in beta stage, the API may change.

Contents:

.. toctree::
   :maxdepth: 2

   intro
   API <api>
   developer

* Web site: http://www.jdhp.org/software_en.html#rpsl
* Online documentation: https://jdhp.gitlab.io/rpsl
* Source code: https://gitlab.com/jdhp/rpi-sensor-lib
* Issue tracker: https://gitlab.com/jdhp/rpi-sensor-lib/issues
* rpsl on PyPI: https://pypi.org/project/rpsl

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Credits
=======

.. include:: ../AUTHORS
   :literal:

License
=======

.. highlight:: none

.. include:: ../LICENSE
   :literal:

